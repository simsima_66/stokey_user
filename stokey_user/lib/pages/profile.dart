import 'package:flutter/material.dart';
import 'package:stokey_user/pages/address.dart';

import 'package:stokey_user/pages/payment.dart';
import 'package:stokey_user/pages/refer_&_earn.dart';
import 'package:stokey_user/pages/wishlist.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  var index;

  get padding => null;

  @override
  Widget buildEmail() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        SizedBox(height: 10),
        Container(
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5),
          ),
          margin: EdgeInsets.only(
            left: 30,
            right: 30,
          ),
          height: 40,
          child: TextField(
            keyboardType: TextInputType.emailAddress,
            style: TextStyle(color: Colors.black87),
            decoration: InputDecoration(
                enabledBorder: new OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(8.0),
                  borderSide: BorderSide(color: Colors.black26),
                ),
                hintText: 'Search',
                suffixIcon:
                    IconButton(icon: Icon(Icons.search, color: Colors.black87)),
                hintStyle: TextStyle(color: Colors.black38)),
          ),
        )
      ],
    );
  }

// class _perofileState extends State<Profile> {
//   @override
  Widget build(BuildContext context) {
    var snapshot;
    return Scaffold(
        appBar: AppBar(
            iconTheme: IconThemeData(
              color: Colors.black,
            ),
            title: Text('Profile',
                style: TextStyle(
                  color: Colors.black,
                )),
            backgroundColor: Colors.white,
            elevation: 4.0),
        body: ListView(children: [
          Container(
            child: Stack(
              children: [
                // SizedBox(height: 40),
                // buildEmail(),
                // SizedBox(height: 50),
                Container(
                  decoration: BoxDecoration(
                      gradient: LinearGradient(colors: [
                    Color(0xFFD63027),
                    Color(0xFFD63027),
                    Color(0xFFAE1D3D),
                    Color(0xFF8E0F50),
                  ])),
                  child: TextButton(
                    style: TextButton.styleFrom(
                      primary: Colors.white,
                      onSurface: Colors.deepOrange[900],
                      padding:
                          EdgeInsets.symmetric(vertical: 30, horizontal: 300),
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => Profile(),
                        ),
                      );
                    },
                    child: null,
                  ),
                ),

                // SizedBox(height: 60),
                buildEmail(),

                Container(
                  // alignment: Alignment.centerRight,
                  padding: EdgeInsets.only(
                    left: 2,
                    right: 2,
                  ),
                )
              ],
            ),
          ),
          SizedBox(height: 20),
          Column(children: [
            Container(
              height: 671,
              color: Colors.white,
              child: Stack(
                children: [
                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                        gradient: LinearGradient(colors: [
                          Color(0xFFD63027),
                          Color(0xFFD63027),
                          Color(0xFFAE1D3D),
                          Color(0xFF8E0F50),
                        ])),
                    margin: EdgeInsets.only(left: 11, right: 10),
                    padding: EdgeInsets.only(
                      left: 12,
                      right: 12,
                    ),
                    height: 200,
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          CircleAvatar(
                              radius: 40.0,
                              backgroundImage: AssetImage(
                                'assets/images/pro_image.png',
                              )),
                          Padding(padding: const EdgeInsets.only(left: 20)),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(children: [
                                    Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: Container(
                                        // padding: const EdgeInsets.only(left: 12, right: 20),
                                        child: Text(
                                          "Name",
                                          style: TextStyle(
                                              fontSize: 25,
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ),
                                    TextButton(
                                      onPressed: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) => Profile(),
                                          ),
                                        );
                                      },

                                      // child: Icons.arrow_forward_ios

                                      //  child:  static const IconData arrow_forward_ios = IconData(0xe5b0, fontFamily: 'MaterialIcons', matchTextDirection: true);
                                    )
                                  ]),

                                  SizedBox(
                                    height: 0,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Container(
                                        child: Text(
                                      "Mobile No",
                                      style: TextStyle(
                                          fontSize: 20, color: Colors.white),
                                    )),
                                  ),

                                  SizedBox(
                                    height: 0,
                                  ),
                                  // Padding(padding: const EdgeInsets.only(left: 15, right: 9)),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Container(
                                        child: Text(
                                      "Mail Id",
                                      style: TextStyle(
                                          fontSize: 20, color: Colors.white),
                                    )),
                                  ),
                                ]),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 30,
                    right: 30,
                    left: 30,
                    child: Container(
                      alignment: Alignment.centerLeft,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Color(0xFFF5F5F5F5),
                      ),
                      // alignment: Alignment.centerRight,

                      margin: EdgeInsets.only(left: 12, right: 10),

                      padding: EdgeInsets.only(
                        left: 2,
                        right: 2,
                      ),

                      height: 500,

                      child: ListView(children: [
                        Padding(
                          padding: const EdgeInsets.all(28.0),
                          child: Row(
                            children: [
                              Expanded(child: Text("Order")),
                              IconButton(
                                icon: Icon(Icons.arrow_forward_ios),
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => Payment(),
                                    ),
                                  );
                                },
                              )
                            ],
                          ),
                        ),

                        Divider(
                          color: Colors.black,
                          height: 30,
                        ),

                        Padding(
                          padding: const EdgeInsets.all(28.0),
                          child: Row(
                            children: [
                              Expanded(child: Text("Wishlist")),
                              IconButton(
                                icon: Icon(Icons.arrow_forward_ios),
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => Wishlist(),
                                    ),
                                  );
                                },
                              )
                            ],
                          ),
                        ),
                        Divider(
                          color: Colors.black,
                          height: 30,
                        ),
                        // Padding(
                        //   padding: const EdgeInsets.all(28.0),
                        //   child: Row(
                        //     children: [
                        //       Expanded(child: Text("Coupons")),
                        //       Icon(Icons.arrow_forward_ios)
                        //     ],
                        //   ),
                        // ),
                        Padding(
                          padding: const EdgeInsets.all(28.0),
                          child: Row(
                            children: [
                              Expanded(child: Text("Coupons")),
                              IconButton(
                                icon: Icon(Icons.arrow_forward_ios),
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => Address()),
                                  );
                                },
                              )
                            ],
                          ),
                        ),
                        Divider(
                          color: Colors.black,
                          height: 30,
                        ),
                        Padding(
                          padding: const EdgeInsets.all(28.0),
                          child: Row(
                            children: [
                              Expanded(child: Text("Address")),
                              IconButton(
                                icon: Icon(Icons.arrow_forward_ios),
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => Address(),
                                    ),
                                  );
                                },
                              )
                            ],
                          ),
                        ),
                        Divider(
                          color: Colors.black,
                          height: 20,
                        ),
                        //
                        Padding(
                          padding: const EdgeInsets.all(28.0),
                          child: Row(
                            children: [
                              Expanded(child: Text("Edit Profile")),
                              IconButton(
                                icon: Icon(Icons.arrow_forward_ios),
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => Address(),
                                    ),
                                  );
                                },
                              )
                            ],
                          ),
                        ),

                        Divider(
                          color: Colors.black,
                          height: 35,
                        ),

                        Padding(
                          padding: const EdgeInsets.all(28.0),
                          child: Row(
                            children: [
                              Expanded(child: Text("Refer & Earn")),
                              IconButton(
                                icon: Icon(Icons.arrow_forward_ios),
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => ReferAndEarn(),
                                    ),
                                  );
                                },
                              )
                            ],
                          ),
                        ),
                        // Padding(
                        //   padding: const EdgeInsets.all(28.0),
                        //   child: Row(
                        //     children: [
                        //       Expanded(child: Text("Refer & Earn")),
                        //       Icon(Icons.arrow_forward_ios)
                        //     ],
                        //   ),
                        // ),
                      ]),
                    ),
                  )
                ],
              ),
            ),
          ]),
        ]));
  }
}

class ProfileListTile extends StatelessWidget {
  final String text;
  const ProfileListTile({
    Key key,
    @required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(28.0),
      child: Row(
        children: [Expanded(child: Text(text)), Icon(Icons.arrow_forward_ios)],
      ),
    );
  }
}
