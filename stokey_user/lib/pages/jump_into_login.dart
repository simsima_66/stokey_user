import 'package:flutter/material.dart';
import 'package:stokey_user/pages/login.dart';

class JumpIntoLogin extends StatefulWidget {
  @override
  _JumpIntoLoginState createState() => _JumpIntoLoginState();
}

class _JumpIntoLoginState extends State<JumpIntoLogin> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            padding: EdgeInsets.only(left: 4, right: 40),
            margin: EdgeInsets.only(left: 80, right: 20),
            child: Text(
              "Congratulations",
              textAlign: TextAlign.start,
              style: TextStyle(
                  fontSize: 25,
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  height: 8),
            ),
          ),
          SizedBox(height: 100),
          Container(
            child: Center(
              child: Image.asset(
                'assets/images/congratulations.png',
                // height: 10,
                // width: 20,
              ),
            ),
          ),
          SizedBox(
            height: 100,
          ),
          Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(colors: [
              Color(0xFFD63027),
              Color(0xFFD63027),
              Color(0xFFAE1D3D),
              Color(0xFF8E0F50),
            ])),
            child: TextButton(
                style: TextButton.styleFrom(
                  primary: Colors.white,

                  // padding: EdgeInsets.only(left: 60, right: 100)
                  padding: EdgeInsets.symmetric(vertical: 2, horizontal: 70),
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Login(),
                    ),
                  );
                },
                child: Text(
                  'JUMP INTO LOGIN'.toUpperCase(),
                  style: TextStyle(
                    fontSize: 15,
                  ),
                )),
          ),
        ],
      ),
    );
  }
}
