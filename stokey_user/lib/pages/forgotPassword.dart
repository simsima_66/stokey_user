import 'package:flutter/material.dart';

import 'package:stokey_user/pages/newPassword.dart';

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

Widget buildEmail() {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.center,
    children: <Widget>[
      SizedBox(height: 10),
      Container(
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5),

          // border: const Border(color: Colors.grey, width: 0.0),

          // boxShadow: [
          //   BoxShadow(
          //       color: Colors.black26, blurRadius: 6, offset: Offset(0, 2))
        ),
        padding: EdgeInsets.only(
          left: 30,
          right: 30,
        ),
        height: 60,
        child: TextField(
          keyboardType: TextInputType.emailAddress,

          // keyboardType: TextInputType.

          style: TextStyle(color: Colors.black87),
          decoration: InputDecoration(
              enabledBorder: new OutlineInputBorder(
                borderRadius: new BorderRadius.circular(8.0),
                borderSide: BorderSide(color: Colors.black26),
              ),
              // contentPadding: EdgeInsets.all(20).0),
              // Padding(
              //   padding:EdgeInsets.all(12.0),
              //   child: buildEmail(),

              // ),
              hintText: 'Email / Mobile No',
              hintStyle: TextStyle(color: Colors.black38)),
        ),
      )
    ],
  );
}

class _ForgotPasswordState extends State<ForgotPassword> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            iconTheme: IconThemeData(
              color: Colors.black,
            ),
            title: Text('Forgot Password',
                style: TextStyle(
                  color: Colors.black,
                )),
            backgroundColor: Colors.white,
            elevation: 4.0),
        body: Container(
          child: Column(
            children: [
              SizedBox(height: 30),
              buildEmail(),
              SizedBox(height: 30),
              Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(colors: [
                  Color(0xFFD63027),
                  Color(0xFFD63027),
                  Color(0xFFAE1D3D),
                  Color(0xFF8E0F50),
                ])),
                child: TextButton(
                    style: TextButton.styleFrom(
                      primary: Colors.white,
                      padding:
                          EdgeInsets.symmetric(vertical: 10, horizontal: 103),
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => NewPassword(),
                        ),
                      );
                    },
                    child: Text(
                      'RESET PASSWORD'.toUpperCase(),
                      style: TextStyle(fontSize: 15),
                    )),
              ),
            ],
          ),
        ));
  }
}

// Widget buildEmail() {
//   return Column(
//     crossAxisAlignment: CrossAxisAlignment.center,
//     children: <Widget>[
//       SizedBox(height: 10),
//       Container(
//         alignment: Alignment.centerLeft,
//         decoration: BoxDecoration(
//           color: Colors.white,
//           borderRadius: BorderRadius.circular(5),

//           // border: const Border(color: Colors.grey, width: 0.0),

//           // boxShadow: [
//           //   BoxShadow(
//           //       color: Colors.black26, blurRadius: 6, offset: Offset(0, 2))
//         ),
//         padding: EdgeInsets.only(
//           left: 30,
//           right: 30,
//         ),
//         height: 60,
//         child: TextField(
//           keyboardType: TextInputType.emailAddress,

//           // keyboardType: TextInputType.

//           style: TextStyle(color: Colors.black87),
//           decoration: InputDecoration(
//               enabledBorder: new OutlineInputBorder(
//                 borderRadius: new BorderRadius.circular(8.0),
//                 borderSide: BorderSide(color: Colors.black26),
//               ),
//               // contentPadding: EdgeInsets.all(20).0),
//               // Padding(
//               //   padding:EdgeInsets.all(12.0),
//               //   child: buildEmail(),

//               // ),
//               hintText: 'Email / Mobile No',
//               hintStyle: TextStyle(color: Colors.black38)),
//         ),
//       )
//     ],
//   );
// }
