import 'package:flutter/material.dart';

class ReferAndEarn extends StatefulWidget {
  @override
  _ReferAndEarnState createState() => _ReferAndEarnState();
}

class _ReferAndEarnState extends State<ReferAndEarn> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.black,
          ),
          backgroundColor: Colors.white,
          title: Text(
            "Refer & Earn",
            style: TextStyle(
              color: Colors.black,
              fontSize: 20,
            ),
          ),
        ),
        body: Container(
            child: Column(
          children: [
            Container(
              height: 25,
              decoration: BoxDecoration(
                  gradient: LinearGradient(colors: [
                Color(0xFFD63027),
                Color(0xFFD63027),
                Color(0xFFAE1D3D),
                Color(0xFF8E0F50),
              ])),
            ),
            Container(
                padding: EdgeInsets.only(left: 20, right: 20),
                child: Column(children: [
                  Image.asset(
                    'assets/images/giftbox.png',
                    width: 500,
                    height: 300,
                  ),
                ])),
            Column(children: [
              Container(
                height: 50,
                padding: EdgeInsets.only(left: 20, right: 20),
                // padding: EdgeInsets.only(bottom: 10),
                child: TextField(
                  // keyboardType: TextInputType.,

                  // keyboardType: TextInputType.

                  style: TextStyle(color: Colors.black87),

                  decoration: InputDecoration(
                      enabledBorder: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(8.0),
                        borderSide: BorderSide(color: Colors.black26),
                      ),
                      hintText: "Your Code",
                      hintStyle: TextStyle(color: Colors.black38)),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    gradient: LinearGradient(colors: [
                      Color(0xFFD63027),
                      Color(0xFFD63027),
                      Color(0xFFAE1D3D),
                      Color(0xFF8E0F50),
                    ])),
                child: TextButton(
                  style: TextButton.styleFrom(
                    padding: EdgeInsets.only(left: 20, right: 20),
                    primary: Colors.white,

                    // backgroundColor: Colors.deepOrange[900],

                    // onSurface: Colors.deepOrange[900],
                    // padding: EdgeInsets.only(top: 2),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ReferAndEarn(),
                      ),
                    );
                  },
                  child: Text(
                    'SHARE'.toUpperCase(),
                    style: TextStyle(fontSize: 10),
                  ),
                ),
                padding: EdgeInsets.only(left: 2, right: 2),
                height: 25,
              ),
              TextButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ReferAndEarn(),
                      ),
                    );
                  },
                  child: Text(
                    "Copy Referal Link",
                    style: TextStyle(
                        decoration: TextDecoration.underline,
                        fontSize: 12,
                        color: Colors.black,
                        letterSpacing: 0.8),
                  )),
            ]),
          ],
        )));
  }
}
