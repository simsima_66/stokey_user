import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:stokey_user/pages/login.dart';
import 'package:stokey_user/pages/newAddress.dart';
import 'package:stokey_user/pages/verifyCode.dart';

class Address extends StatefulWidget {
  @override
  _AddressState createState() => _AddressState();
}

int group = 1;

class _AddressState extends State<Address> {
  String title = 'SingleChildScrollView';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.black,
          ),
          title: Text('Address',
              style: TextStyle(
                color: Colors.black,
              )),
          backgroundColor: Colors.white,
          elevation: 4.0),
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          child: Stack(
            children: <Widget>[
              Container(
                height: double.infinity,
                width: double.infinity,
                child: SingleChildScrollView(
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(height: 20),
                        buildEmail(),
                        SizedBox(height: 20)
                      ]),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildEmail() {
    return Column(crossAxisAlignment: CrossAxisAlignment.center, children: <
        Widget>[
      SizedBox(height: 1),
      Container(
        alignment: Alignment.centerLeft,

        margin: EdgeInsets.all(23),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15),
            border: Border.all(color: Colors.black26)),
        padding: EdgeInsets.only(
          left: 20,
          right: 20,
        ),
        height: 60,

        // child: TextField(
        //   keyboardType: TextInputType.emailAddress,

        child: TextButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => NewAddress(),
                ),
              );
            },
            child: Text(
              "+ Add New Address".toString(),
              style: TextStyle(
                  fontSize: 15, color: Colors.black, letterSpacing: 1),
              textAlign: TextAlign.right,
            )

            // contentPadding: EdgeInsets.all(20).0),
            // Padding(
            //   padding:EdgeInsets.all(12.0),
            //   child: buildEmail(),

            // ),
            ),
      ),
      SizedBox(height: 20),
      Column(children: [
        Container(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Row(
                  children: [
                    Radio(
                      value: 1,
                      groupValue: group,
                      onChanged: (T) {
                        print(T);

                        setState(() {
                          group = T;
                        });
                      },
                    ),
                    Expanded(
                      child: Text('Home',
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 16)),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(colors: [
                        Color(0xFFD63027),
                        Color(0xFFD63027),
                        Color(0xFFAE1D3D),
                        Color(0xFF8E0F50),
                      ])),
                      child: TextButton(
                          style: TextButton.styleFrom(
                              primary: Colors.white,
                              padding: EdgeInsets.only(left: 10, right: 10)),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => Address(),
                              ),
                            );
                          },
                          child: Text(
                            'Edit'.toString(),
                            style: TextStyle(fontSize: 15),
                          )),
                    ),
                  ],
                ),
              )
            ],
          ),
          height: 200,
          // color: Colors.white,

          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(blurRadius: 20, color: Colors.black.withOpacity(0.2))
            ],

            // border: Border.all(color: Colors.white),
            borderRadius: BorderRadius.all(Radius.circular(20)),
            color: Colors.white,
          ),
          margin: EdgeInsets.only(left: 22, right: 22),
        ),
        SizedBox(height: 20),
        Column(children: [
          Container(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Row(
                    children: [
                      Radio(
                        value: 2,
                        groupValue: group,
                        onChanged: (T) {
                          print(T);

                          setState(() {
                            group = T;
                          });
                        },
                      ),
                      Expanded(
                        child: Text('Home',
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: 16)),
                      ),
                      Container(
                        decoration: BoxDecoration(
                            gradient: LinearGradient(colors: [
                          Color(0xFFD63027),
                          Color(0xFFD63027),
                          Color(0xFFAE1D3D),
                          Color(0xFF8E0F50),
                        ])),
                        child: TextButton(
                            style: TextButton.styleFrom(
                                primary: Colors.white,
                                padding: EdgeInsets.only(left: 15, right: 15)),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => Address(),
                                ),
                              );
                            },
                            child: Text(
                              'Edit'.toString(),
                              style: TextStyle(fontSize: 15),
                            )),
                      )
                    ],
                  ),
                )
              ],
            ),

            height: 200,
            // color: Colors.white,

            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(blurRadius: 20, color: Colors.black.withOpacity(0.2))
              ],

              // border: Border.all(color: Colors.white),
              borderRadius: BorderRadius.all(Radius.circular(20)),
              color: Colors.white,
            ),
            margin: EdgeInsets.only(left: 22, right: 22),
          ),
          SizedBox(height: 20),
          Column(children: [
            Container(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Row(
                      children: [
                        Radio(
                          value: 3,
                          groupValue: group,
                          onChanged: (T) {
                            print(T);

                            setState(() {
                              group = T;
                            });
                          },
                        ),
                        Expanded(
                          child: Text('Office',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16)),
                        ),
                        Container(
                          decoration: BoxDecoration(
                              gradient: LinearGradient(colors: [
                            Color(0xFFD63027),
                            Color(0xFFD63027),
                            Color(0xFFAE1D3D),
                            Color(0xFF8E0F50),
                          ])),
                          child: TextButton(
                              style: TextButton.styleFrom(
                                  primary: Colors.white,
                                  padding:
                                      EdgeInsets.only(left: 15, right: 15)),
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => Address(),
                                  ),
                                );
                              },
                              child: Text(
                                'Edit'.toString(),
                                style: TextStyle(fontSize: 15),
                              )),
                        )
                      ],
                    ),
                  )
                ],
              ),
              height: 200,
              // color: Colors.white,

              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      blurRadius: 20, color: Colors.black.withOpacity(0.2))
                ],

                // border: Border.all(color: Colors.white),
                borderRadius: BorderRadius.all(Radius.circular(20)),
                color: Colors.white,
              ),
              margin: EdgeInsets.only(left: 22, right: 22),
            ),
          ])
        ])
      ]),
      SizedBox(height: 20),
      Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(colors: [
          Color(0xFFD63027),
          Color(0xFFD63027),
          Color(0xFFAE1D3D),
          Color(0xFF8E0F50),
        ])),
        child: TextButton(
            style: TextButton.styleFrom(
              primary: Colors.white,
              padding: EdgeInsets.symmetric(vertical: 12, horizontal: 130),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => (Address()),
                ),
              );
            },
            child: Text(
              'DELIVER HERE'.toUpperCase(),
              style: TextStyle(fontSize: 15),
            )),
      )
    ]);
  }
}
// height: 200

// SizedBox(height: 20),
// Column(children: [
//   Container(
//       height: 200,
//       color: Colors.white,
//       child: Container(
//           decoration: BoxDecoration(
//             boxShadow: [
//               BoxShadow(
//                   blurRadius: 20,
//                   color: Colors.black.withOpacity(0.2))
//             ],

//             borderRadius: BorderRadius.all(Radius.circular(20)),
//             color: Colors.white,
//           ),
//           margin: EdgeInsets.only(left: 11, right: 10),
//           padding: EdgeInsets.only(
//             left: 160,
//             right: 160,
//           ),
//           height: 50,
//           child: Padding(padding: const EdgeInsets.all(15.0)))),
//   SizedBox(height: 20),
//   Column(children: [
//     Container(
//         height: 200,
//         color: Colors.white,
//         child: Container(
//             decoration: BoxDecoration(
//               boxShadow: [
//                 BoxShadow(
//                     blurRadius: 20,
//                     color: Colors.black.withOpacity(0.2))
//               ],

//               borderRadius: BorderRadius.all(Radius.circular(20)),
//               color: Colors.white,
//             ),
//             margin: EdgeInsets.only(left: 11, right: 10),
//             padding: EdgeInsets.only(
//               left: 160,
//               right: 160,
//             ),
//             height: 50,
//             child: Padding(padding: const EdgeInsets.all(15.0)))),
//   ])
