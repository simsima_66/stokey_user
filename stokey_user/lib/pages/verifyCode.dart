import 'package:flutter/material.dart';
import 'package:stokey_user/pages/jump_into_login.dart';
import 'package:stokey_user/pages/profile.dart';

class VerifyCode extends StatefulWidget {
  @override
  _VerifyCodeState createState() => _VerifyCodeState();
}

class _VerifyCodeState extends State<VerifyCode> {
  String code = "";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.black,
          ),
          backgroundColor: Colors.white,
          title: Text(
            'Verify Code',
            style: TextStyle(color: Colors.black),
          ),
        ),
        body: Container(
            child: Column(children: [
          SizedBox(
            height: 80,
          ),
          Padding(
              padding: const EdgeInsets.only(left: 30),
              child: Column(
                children: [
                  Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        buildCodeNumberBox(
                            code.length > 0 ? code.substring(0, 1) : ""),
                        Padding(padding: const EdgeInsets.only(left: 20)),
                        buildCodeNumberBox(
                            code.length > 1 ? code.substring(1, 2) : ""),
                        Padding(padding: const EdgeInsets.only(left: 20)),
                        buildCodeNumberBox(
                            code.length > 2 ? code.substring(2, 3) : ""),
                        Padding(padding: const EdgeInsets.only(left: 20)),
                        buildCodeNumberBox(
                            code.length > 3 ? code.substring(3, 4) : ""),
                        Padding(padding: const EdgeInsets.only(left: 20)),
                      ]),
                  SizedBox(
                    height: 25,
                  ),
                  Container(
                    alignment: Alignment.centerRight,
                    padding: EdgeInsets.only(
                      left: 110,
                      right: 150,
                    ),
                    child: Text(
                      " Resend",
                      style: TextStyle(
                        decoration: TextDecoration.underline,
                        fontSize: 17,
                        color: Colors.red[900],
                        letterSpacing: 0.8,
                      ),
                    ),
                  )
                ],
              )),
          SizedBox(height: 20),
          Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(colors: [
              Color(0xFFD63027),
              Color(0xFFD63027),
              Color(0xFFAE1D3D),
              Color(0xFF8E0F50),
            ])),
            child: TextButton(
                style: TextButton.styleFrom(
                  primary: Colors.white,
                  padding: EdgeInsets.symmetric(vertical: 12, horizontal: 130),
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => JumpIntoLogin(),
                    ),
                  );
                },
                child: Text(
                  'VERIFY'.toUpperCase(),
                  style: TextStyle(fontSize: 15),
                )),
          )
        ])));

    // Container(

    //   NumericPad(

    //     onNumberSelected: (value){
    //       setState(() {
    //         if(value != -1){
    //           if(code.length < 4){
    //             code = code + value.tostring();
    //           }
    //         }
    //         else{
    //           code = code.substring(0, code.length - 1);
    //         }

    //       });

    //     }

    //   )

    // );
  }

  Widget buildCodeNumberBox(String codeNumber) {
    return Padding(
      padding: EdgeInsets.only(right: 1, left: 1),
      child: SizedBox(
        width: 60,
        height: 60,
        child: Container(
          decoration: BoxDecoration(
              color: Color(0xFFF6F5FA),
              borderRadius: BorderRadius.all(
                Radius.circular(15),
              ),
              boxShadow: <BoxShadow>[
                BoxShadow(
                  color: Colors.black26,
                  blurRadius: 25.0,
                  spreadRadius: 1,
                  offset: Offset(0.0, 0.75),
                ),
              ]),
          child: Center(
              child: Text(
            codeNumber,
            style: TextStyle(
              fontSize: 22,
              fontWeight: FontWeight.bold,
              color: Color(0xFF1F1F1F),
            ),
          )),
        ),
      ),
    );
  }
}

class Textalign {}

buildCodeNumberBox(String s) {}
