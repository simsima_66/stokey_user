import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:stokey_user/pages/login.dart';
import 'package:stokey_user/pages/verifyCode.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            iconTheme: IconThemeData(
              color: Colors.black,
            ),
            title: Text('Sign Up',
                style: TextStyle(
                  color: Colors.black,
                )),
            backgroundColor: Colors.white,
            elevation: 4.0),
        body: AnnotatedRegion<SystemUiOverlayStyle>(
            value: SystemUiOverlayStyle.light,
            child: GestureDetector(
                child: Stack(children: <Widget>[
              Container(
                height: double.infinity,
                width: double.infinity,
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(height: 50),
                      buildEmail(),
                      Password(),
                      SizedBox(height: 20),
                      Container(
                          alignment: Alignment.centerRight,
                          padding: EdgeInsets.only(
                            left: 30,
                            right: 30,
                          ),
                          height: 25,
                          child: Row(
                            children: [
                              Checkbox(
                                value: false,
                                onChanged: (bool value) {
                                  print(value);
                                },
                              ),
                              Container(
                                alignment: Alignment.center,
                                child: Text(
                                  "I agree to the terms and conditions and \n"
                                          " privacy policy"
                                      .toString(),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 10,
                                      color: Colors.black38,
                                      letterSpacing: 1),
                                  // textAlign: TextAlign.right,
                                ),
                              ),
                              // Checkbox(
                              //   value: false,
                              //   onChanged: (bool value) {
                              //     print(value);
                              //   },
                              // )
                            ],
                          )

                          //   style: TextStyle(

                          //       fontSize: 10,
                          //       color: Colors.black38,
                          //       letterSpacing: 1),
                          //   textAlign: TextAlign.right,
                          // ),

                          ),
                      SizedBox(height: 30),
                      Container(
                        decoration: BoxDecoration(
                            gradient: LinearGradient(colors: [
                          Color(0xFFD63027),
                          Color(0xFFD63027),
                          Color(0xFFAE1D3D),
                          Color(0xFF8E0F50),
                        ])),
                        child: TextButton(
                            style: TextButton.styleFrom(
                              primary: Colors.white,
                              padding: EdgeInsets.symmetric(
                                  vertical: 10, horizontal: 142),
                            ),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => VerifyCode(),
                                ),
                              );
                            },
                            child: Text(
                              'REGISTER'.toUpperCase(),
                              style: TextStyle(fontSize: 15),
                            )),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 100),
                        child: Row(children: [
                          Text(
                            "Already have an account?",
                            style: TextStyle(
                                fontSize: 12,
                                color: Colors.black38,
                                letterSpacing: 1),
                          ),
                          TextButton(
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => Login(),
                                  ),
                                );
                              },
                              child: Text(
                                "Login",
                                style: TextStyle(
                                    decoration: TextDecoration.underline,
                                    fontSize: 12,
                                    color: Colors.red[900],
                                    letterSpacing: 0.8),
                              )),
                        ]),
                      ),
                    ],
                  ),
                ),
              ),
            ]))));
  }
}

// ],

Widget buildEmail() {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.center,
    children: <Widget>[
      SizedBox(height: 10),
      Container(
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5),
        ),
        padding: EdgeInsets.only(
          left: 30,
          right: 30,
        ),
        height: 60,
        child: TextField(
          keyboardType: TextInputType.emailAddress,
          style: TextStyle(color: Colors.black87),
          decoration: InputDecoration(
              enabledBorder: new OutlineInputBorder(
                borderRadius: new BorderRadius.circular(8.0),
                borderSide: BorderSide(color: Colors.black26),
              ),
              // contentPadding: EdgeInsets.all(20).0),
              // Padding(
              //   padding:EdgeInsets.all(12.0),
              //   child: buildEmail(),

              // ),
              hintText: 'Email / Mobile No',
              hintStyle: TextStyle(color: Colors.black38)),
        ),
      )
    ],
  );
}

class Password extends StatefulWidget {
  @override
  _PasswordState createState() => _PasswordState();
}

class _PasswordState extends State<Password> {
  bool _isObscure = true;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        SizedBox(height: 10),
        Container(
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5),

            // border: const Border(color: Colors.grey, width: 0.0),

            // boxShadow: [
            //   BoxShadow(
            //       color: Colors.black26, blurRadius: 6, offset: Offset(0, 2))
          ),
          padding: EdgeInsets.only(
            left: 30,
            right: 30,
          ),
          height: 60,
          child: TextField(
            obscureText: _isObscure,
            // keyboardType: TextInputType.,

            // keyboardType: TextInputType.

            style: TextStyle(color: Colors.black87),

            decoration: InputDecoration(
                enabledBorder: new OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(8.0),
                  borderSide: BorderSide(color: Colors.black26),
                ),
                hintText: 'Password',
                suffixIcon: IconButton(
                    icon: Icon(
                        _isObscure ? Icons.visibility : Icons.visibility_off),
                    onPressed: () {
                      this.setState(() {
                        _isObscure = !_isObscure;
                      });
                    }),
                hintStyle: TextStyle(color: Colors.black38)),
          ),
        ),
      ],
    );
  }
}
