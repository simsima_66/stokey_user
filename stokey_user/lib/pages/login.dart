import 'package:flutter/material.dart';
// import 'package:flutter/semantics.dart';
import 'package:flutter/services.dart';
import 'package:stokey_user/pages/forgotPassword.dart';
import 'package:stokey_user/pages/profile.dart';
import 'package:stokey_user/pages/signup.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

Widget buildEmail() {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.center,
    children: <Widget>[
      SizedBox(height: 10),
      Container(
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5),
        ),
        padding: EdgeInsets.only(
          left: 30,
          right: 30,
        ),
        height: 60,
        child: TextField(
          keyboardType: TextInputType.emailAddress,
          style: TextStyle(color: Colors.black87),
          decoration: InputDecoration(
              enabledBorder: new OutlineInputBorder(
                borderRadius: new BorderRadius.circular(8.0),
                borderSide: BorderSide(color: Colors.black26),
              ),
              // contentPadding: EdgeInsets.all(20).0),
              // Padding(
              //   padding:EdgeInsets.all(12.0),
              //   child: buildEmail(),

              // ),
              hintText: 'Email / Mobile No',
              hintStyle: TextStyle(color: Colors.black38)),
        ),
      )
    ],
  );
}

class Password extends StatefulWidget {
  @override
  _PasswordState createState() => _PasswordState();
}

class _PasswordState extends State<Password> {
  bool _isObscure = true;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        SizedBox(height: 10),
        Container(
          alignment: Alignment.centerLeft,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5),

            // border: const Border(color: Colors.grey, width: 0.0),

            // boxShadow: [
            //   BoxShadow(
            //       color: Colors.black26, blurRadius: 6, offset: Offset(0, 2))
          ),
          padding: EdgeInsets.only(
            left: 30,
            right: 30,
          ),
          height: 60,
          child: TextField(
            obscureText: _isObscure,
            // keyboardType: TextInputType.,

            // keyboardType: TextInputType.

            style: TextStyle(color: Colors.black87),

            decoration: InputDecoration(
                enabledBorder: new OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(8.0),
                  borderSide: BorderSide(color: Colors.black26),
                ),
                hintText: 'Password',
                suffixIcon: IconButton(
                    icon: Icon(
                        _isObscure ? Icons.visibility : Icons.visibility_off),
                    onPressed: () {
                      this.setState(() {
                        _isObscure = !_isObscure;
                      });
                    }),
                hintStyle: TextStyle(color: Colors.black38)),
          ),
        ),
      ],
    );
  }
}

class _LoginState extends State<Login> {
  bool isHiddenPassword = true;
  bool _isObscure = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: AnnotatedRegion<SystemUiOverlayStyle>(
            value: SystemUiOverlayStyle.light,
            child: GestureDetector(
                child: Stack(children: <Widget>[
              Container(
                height: double.infinity,
                width: double.infinity,
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(height: 50),
                      Container(
                        child: Center(
                          child: Image.asset(
                            'assets/images/logoo.png',
                            height: 100,
                            width: 200,
                          ),
                        ),
                      ),
                      SizedBox(height: 50),
                      // SizedBox(height: 50),
                      // buildEmail(),
                      // Password(),
                      Text(
                        'Login',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),

                      SizedBox(height: 30),

                      buildEmail(),
                      Password(),
                      SizedBox(height: 20),
                      Container(
                        alignment: Alignment.centerRight,
                        padding: EdgeInsets.only(
                          left: 30,
                          right: 30,
                        ),
                        height: 40,
                        child: TextButton(
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => ForgotPassword(),
                              ),
                            );
                          },
                          child: Text(
                            "Forgot Password?".toString(),
                            style: TextStyle(
                                fontSize: 10,
                                color: Colors.black,
                                letterSpacing: 1),
                            textAlign: TextAlign.right,
                          ),
                        ),
                      ),
                      SizedBox(height: 30),

                      Container(
                        decoration: BoxDecoration(
                            gradient: LinearGradient(colors: [
                          Color(0xFFD63027),
                          Color(0xFFD63027),
                          Color(0xFFAE1D3D),
                          Color(0xFF8E0F50),
                        ])),
                        child: TextButton(
                            style: TextButton.styleFrom(
                              primary: Colors.white,
                              // backgroundColor: Colors.deepOrange[900],

                              // onSurface: Colors.deepOrange[900],
                              padding: EdgeInsets.symmetric(
                                  vertical: 10, horizontal: 142),
                            ),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => Profile(),
                                ),
                              );
                            },
                            child: Text(
                              'LOGIN'.toUpperCase(),
                              style: TextStyle(fontSize: 15),
                            )),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 100),
                        child: Row(children: [
                          Text(
                            "Don't have an account?",
                            style: TextStyle(
                                fontSize: 12,
                                color: Colors.black38,
                                letterSpacing: 1),
                          ),
                          TextButton(
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => SignUp(),
                                  ),
                                );
                              },
                              child: Text(
                                "Register",
                                style: TextStyle(
                                    decoration: TextDecoration.underline,
                                    fontSize: 12,
                                    color: Colors.black,
                                    letterSpacing: 0.8),
                              )),
                        ]),
                      ),
                    ],
                  ),
                ),
              ),
            ]))));
  }
}
