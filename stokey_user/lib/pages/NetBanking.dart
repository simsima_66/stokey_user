import 'package:flutter/material.dart';
import 'package:stokey_user/pages/address.dart';

// stores ExpansionPanel state information
class Item {
  Item({
    @required this.expandedValue,
    @required this.headerValue,
    this.isExpanded = false,
  });

  Widget expandedValue;
  String headerValue;
  bool isExpanded;
}

List<Item> generateItems(int numberOfItems) {
  return List<Item>.generate(numberOfItems, (int index) {
    return Item(
      headerValue: 'NET BANKING',
      expandedValue: Text(
        "netbanking",
        style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold,
            color: Colors.black38,
            letterSpacing: 0),
      ),
    );
  });
}

/// This is the stateful widget that the main application instantiates.
class NetBanking extends StatefulWidget {
  const NetBanking({Key key}) : super(key: key);

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

/// This is the private State class that goes with CashOnDelivery.
class _MyStatefulWidgetState extends State<NetBanking> {
  final List<Item> _data = generateItems(1);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: _buildPanel(),
      ),
    );
  }

  Widget _buildPanel() {
    return ExpansionPanelList(
      expansionCallback: (int index, bool isExpanded) {
        setState(() {
          _data[index].isExpanded = !isExpanded;
        });
      },
      children: _data.map<ExpansionPanel>((Item item) {
        return ExpansionPanel(
          headerBuilder: (BuildContext context, bool isExpanded) {
            return Container(
              padding: EdgeInsets.symmetric(horizontal: 7),
              child: Row(
                children: [
                  Image.asset(
                    'assets/images/Dollar.png',
                    height: 25,
                    width: 25,
                  ),
                  SizedBox(
                    width: 2,
                  ),
                  Text(
                    'NET BANKING',
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                  )
                ],
              ),
            );
          },
          body: Container(
            child: Column(
              children: [
                // Text(
                //   '234542',
                //   style: TextStyle(fontSize: 70),
                // ),
                // Container(
                //   padding: EdgeInsets.only(left: 40, right: 40),
                //   // padding: EdgeInsets.only(bottom: 10),
                //   child: TextField(
                //     // keyboardType: TextInputType.,

                //     // keyboardType: TextInputType.

                //     style: TextStyle(color: Colors.black87),

                //     decoration: InputDecoration(
                //         enabledBorder: new OutlineInputBorder(
                //           borderRadius: new BorderRadius.circular(8.0),
                //           borderSide: BorderSide(color: Colors.black26),
                //         ),
                //         hintText: "Enter Code Number",
                //         hintStyle: TextStyle(color: Colors.black38)),
                //   ),
                // ),
                // SizedBox(height: 10),
                // Row(
                //   children: [
                //     Padding(padding: EdgeInsets.only(left: 100)),
                //     Text(
                //       "CANCEL",
                //       style: TextStyle(
                //           fontSize: 18,
                //           fontWeight: FontWeight.bold,
                //           color: Colors.black38,
                //           letterSpacing: 0),
                //     ),
                //     SizedBox(
                //       width: 20,
                //     ),
                //     TextButton(
                //       onPressed: () {
                //         Navigator.push(
                //           context,
                //           MaterialPageRoute(
                //             builder: (context) => PhonePe(),
                //           ),
                //         );
                //       },
                //       child: Container(

                //           // color: Colors.white,
                //           height: 40,
                //           child: Center(
                //             child: Text(
                //               "OK",
                //               style: TextStyle(
                //                   color: Colors.white,
                //                   fontWeight: FontWeight.bold,
                //                   fontSize: 18),
                //             ),
                //           ),
                //           padding:
                //               EdgeInsets.symmetric(horizontal: 15, vertical: 6),
                //           decoration: BoxDecoration(
                //             borderRadius: BorderRadius.circular(6),
                //             color: Colors.grey,
                //           )),
                //     )
                //   ],
                // ),
                SizedBox(
                  height: 1,
                ),
                Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 50),
                    ),
                    Radio(
                      value: 1,
                      groupValue: group,
                      onChanged: (T) {
                        print(T);

                        setState(() {
                          group = T;
                        });
                      },
                    ),

                    Image.asset(
                      'assets/images/axis.png',
                      height: 20,
                      width: 20,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(child: Text('Axis Bank')),

                    // IconButton(
                    //   icon: Icon(Icons.arrow_forward_ios),
                    // onPressed: () {
                    //   Navigator.push(
                    //     context,
                    //     MaterialPageRoute(
                    //       builder: (context) => PhonePe(),
                    //     ),
                    //   );
                    // },
                    // )
                  ],
                ),

                Divider(
                  color: Colors.black,
                  height: 5,
                ),
                SizedBox(
                  height: 5,
                ),
                Padding(
                  padding: const EdgeInsets.all(28.0),
                  child: Row(
                    children: [
                      SizedBox(
                        height: 5,
                      ),
                      SizedBox(
                        width: 23,
                      ),

                      Radio(
                        value: 2,
                        groupValue: group,
                        onChanged: (T) {
                          print(T);

                          setState(() {
                            group = T;
                          });
                        },
                      ),

                      Image.asset(
                        'assets/images/SIB.png',
                        height: 25,
                        width: 25,
                      ),

                      Padding(padding: EdgeInsets.only(left: 10)),

                      SizedBox(
                        width: 2,
                      ),
                      Expanded(child: Text("South Indian Bank")),
                      // IconButton(
                      //   icon: Icon(Icons.arrow_forward_ios),
                      //   onPressed: () {
                      //     Navigator.push(
                      //       context,
                      //       MaterialPageRoute(
                      //         builder: (context) => PhonePe(),
                      //       ),
                      //     );
                      //   },
                      // )
                    ],
                  ),
                ),

                SizedBox(
                  height: 2,
                ),

                Padding(padding: EdgeInsets.only(bottom: 2)),
                Divider(
                  color: Colors.black,
                  height: 5,
                ),
                // Padding(
                //   padding: const EdgeInsets.all(28.0),
                //   child: Row(
                //     children: [
                //       Expanded(child: Text("Coupons")),
                //       Icon(Icons.arrow_forward_ios)
                //     ],
                //   ),
                // ),
                Padding(
                  padding: const EdgeInsets.only(left: 50),
                  child: Row(
                    children: [
                      Radio(
                        value: 3,
                        groupValue: group,
                        onChanged: (T) {
                          print(T);

                          setState(() {
                            group = T;
                          });
                        },
                      ),

                      Image.asset(
                        'assets/images/sbi.png',
                        height: 25,
                        width: 25,
                      ),

                      Padding(padding: EdgeInsets.only(left: 10)),
                      Expanded(child: Text("State Bank Of India")),
                      //   IconButton(
                      //     icon: Icon(Icons.arrow_forward_ios),
                      //     onPressed: () {
                      //       Navigator.push(
                      //         context,
                      //         MaterialPageRoute(
                      //           builder: (context) => PhonePe(),
                      //         ),
                      //       );
                      //     },
                      //   )
                    ],
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 40),
                )
              ],
            ),

            // margin: EdgeInsets.only(left: 4, right: 3),
          ),
          isExpanded: item.isExpanded,
        );
      }).toList(),
    );
  }
}
