import 'package:flutter/material.dart';

class NewAddress extends StatefulWidget {
  @override
  _NewAddressState createState() => _NewAddressState();
}

class _NewAddressState extends State<NewAddress> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.black,
          ),
          backgroundColor: Colors.white,
          title: Text(
            'New Address',
            style: TextStyle(color: Colors.black),
          ),
        ),
        body: ListView(children: [
          Container(
            height: 740,
            // color: Colors.white,
            child: Stack(children: [
              Container(
                height: 70,
                decoration: BoxDecoration(
                    // borderRadius: BorderRadius.all(Radius.circular(20)),
                    gradient: LinearGradient(colors: [
                  Color(0xFFD63027),
                  Color(0xFFD63027),
                  Color(0xFFAE1D3D),
                  Color(0xFF8E0F50),
                ])),
                // margin: EdgeInsets.only(left: 11, right: 10),
                // padding: EdgeInsets.only(
                // left: 8,
                // right: 8,
                // ),
                // height: 200,
                // child: Padding(
                //   padding: const EdgeInsets.all(15.0),
                //   child: Row(
                //     crossAxisAlignment: CrossAxisAlignment.start,
                //     children: [
                //       CircleAvatar(
                //           radius: 40.0,
                //           backgroundImage: AssetImage(
                //             'assets/images/pro_image.png',
                //           )),
                //       Padding(padding: const EdgeInsets.only(left: 20)),
                //       Padding(
                //         padding: const EdgeInsets.all(8.0),
                //         child: Column(
                //             crossAxisAlignment: CrossAxisAlignment.start,
                //             children: <Widget>[
                //               Row(children: [
                //                 Padding(
                //                   padding: const EdgeInsets.all(10.0),
                //                   child: Container(
                //                     // padding: const EdgeInsets.only(left: 12, right: 20),
                //                     child: Text(
                //                       "Name",
                //                       style: TextStyle(
                //                           fontSize: 25,
                //                           color: Colors.white,
                //                           fontWeight: FontWeight.bold),
                //                     ),
                //                   ),
              ),

              Positioned(
                  bottom: 20,
                  right: 0,
                  left: 0,
                  child: Container(
                    alignment: Alignment.centerLeft,

                    child: Column(
                      children: <Widget>[
                        SizedBox(height: 30),
                        Container(
                          alignment: Alignment.centerLeft,
                          decoration: BoxDecoration(
                            // color: Colors.white,
                            borderRadius: BorderRadius.circular(5),
                          ),
                          height: 40,
                          child: TextField(
                            keyboardType: TextInputType.emailAddress,
                            style: TextStyle(color: Colors.black87),
                            decoration: InputDecoration(
                                enabledBorder: new OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(8.0),
                                  borderSide: BorderSide(color: Colors.black26),
                                ),
                                // contentPadding: EdgeInsets.all(20).0),
                                // Padding(
                                //   padding:EdgeInsets.all(12.0),
                                //   child: buildEmail(),

                                // ),
                                hintText: 'Full Name',
                                suffixIcon: IconButton(
                                    icon: Icon(Icons.account_circle_outlined,
                                        color: Colors.black26)),
                                hintStyle: TextStyle(color: Colors.black38)),
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Container(
                          height: 40,
                          child: TextField(
                            keyboardType: TextInputType.emailAddress,
                            style: TextStyle(color: Colors.black87),
                            decoration: InputDecoration(
                                enabledBorder: new OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(8.0),
                                  borderSide: BorderSide(color: Colors.black26),
                                ),

                                // contentPadding: EdgeInsets.all(20).0),
                                // Padding(
                                //   padding:EdgeInsets.all(12.0),
                                //   child: buildEmail(),

                                // ),
                                hintText: 'Phone Number',
                                suffixIcon: IconButton(
                                    icon: Icon(Icons.phone,
                                        color: Colors.black26)),
                                hintStyle: TextStyle(color: Colors.black38)),
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Container(
                          height: 40,
                          child: TextField(
                            keyboardType: TextInputType.emailAddress,
                            style: TextStyle(color: Colors.black87),
                            decoration: InputDecoration(
                                enabledBorder: new OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(8.0),
                                  borderSide: BorderSide(color: Colors.black26),
                                ),
                                // contentPadding: EdgeInsets.all(20).0),
                                // Padding(
                                //   padding:EdgeInsets.all(12.0),
                                //   child: buildEmail(),

                                // ),
                                hintText: 'Alternate Phone Number',
                                suffixIcon: IconButton(
                                    icon: Icon(Icons.phone,
                                        color: Colors.black26)),
                                hintStyle: TextStyle(color: Colors.black38)),
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Row(children: [
                          new Flexible(
                            child: Container(
                              height: 40,

                              child: TextField(
                                keyboardType: TextInputType.emailAddress,
                                style: TextStyle(color: Colors.black87),
                                decoration: InputDecoration(
                                    enabledBorder: new OutlineInputBorder(
                                      borderRadius:
                                          new BorderRadius.circular(8.0),
                                      borderSide:
                                          BorderSide(color: Colors.black26),
                                    ),
                                    // contentPadding: EdgeInsets.all(20).0),
                                    // Padding(
                                    //   padding:EdgeInsets.all(12.0),
                                    //   child: buildEmail(),

                                    // ),
                                    hintText: 'Pincode',
                                    hintStyle:
                                        TextStyle(color: Colors.black38)),
                              ),
                              // padding: EdgeInsets.only(left: 1, right: 9),

                              // margin: EdgeInsets.only(right: 160),
                            ),
                          ),
                          SizedBox(
                            width: 20,
                          ),
                          new Flexible(
                              child: Container(
                            height: 38,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                gradient: LinearGradient(colors: [
                                  Color(0xFFD63027),
                                  Color(0xFFD63027),
                                  Color(0xFFAE1D3D),
                                  Color(0xFF8E0F50),
                                ])),
                            padding: EdgeInsets.only(
                              left: 4,
                              right: 0,
                            ),
                            child: Row(
                              children: [
                                Flexible(
                                    child: Text("Use My Location",
                                        style: TextStyle(color: Colors.white))),
                                SizedBox(
                                  width: 12,
                                ),
                                Icon(
                                  Icons.gps_fixed,
                                  color: Colors.white,
                                )
                              ],
                            ),
                            //  color: Colors.deepOrange[900],
                          ))
                        ]),
                        SizedBox(
                          height: 30,
                        ),
                        Row(children: [
                          new Flexible(
                            child: Container(
                              height: 40,

                              child: TextField(
                                keyboardType: TextInputType.emailAddress,
                                style: TextStyle(color: Colors.black87),
                                decoration: InputDecoration(
                                    enabledBorder: new OutlineInputBorder(
                                      borderRadius:
                                          new BorderRadius.circular(8.0),
                                      borderSide:
                                          BorderSide(color: Colors.black26),
                                    ),
                                    // contentPadding: EdgeInsets.all(20).0),
                                    // Padding(
                                    //   padding:EdgeInsets.all(12.0),
                                    //   child: buildEmail(),

                                    // ),
                                    hintText: 'State',
                                    hintStyle:
                                        TextStyle(color: Colors.black38)),
                              ),
                              // padding: EdgeInsets.only(left: 1, right: 9),

                              // margin: EdgeInsets.only(right: 160),
                            ),
                          ),
                          SizedBox(width: 20),
                          new Flexible(
                            child: Container(
                              height: 40,

                              child: TextField(
                                keyboardType: TextInputType.emailAddress,
                                style: TextStyle(color: Colors.black87),
                                decoration: InputDecoration(
                                    enabledBorder: new OutlineInputBorder(
                                      borderRadius:
                                          new BorderRadius.circular(8.0),
                                      borderSide:
                                          BorderSide(color: Colors.black26),
                                    ),
                                    // contentPadding: EdgeInsets.all(20).0),
                                    // Padding(
                                    //   padding:EdgeInsets.all(12.0),
                                    //   child: buildEmail(),

                                    // ),

                                    suffixIcon: IconButton(
                                        icon: Icon(Icons.location_city,
                                            color: Colors.black26)),
                                    hintText: 'City',
                                    hintStyle:
                                        TextStyle(color: Colors.black38)),
                              ),
                              // padding: EdgeInsets.only(left: 1, right: 9),

                              // margin: EdgeInsets.only(right: 160),
                            ),
                          ),
                        ]),
                        SizedBox(
                          height: 25,
                        ),
                        Container(
                          height: 40,
                          child: TextField(
                            keyboardType: TextInputType.emailAddress,
                            style: TextStyle(color: Colors.black87),
                            decoration: InputDecoration(
                                enabledBorder: new OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(8.0),
                                  borderSide: BorderSide(color: Colors.black26),
                                ),
                                // contentPadding: EdgeInsets.all(20).0),
                                // Padding(
                                //   padding:EdgeInsets.all(12.0),
                                //   child: buildEmail(),

                                // ),
                                hintText: 'House No,Building Name',
                                hintStyle: TextStyle(color: Colors.black38)),
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Container(
                          height: 40,
                          child: TextField(
                            keyboardType: TextInputType.emailAddress,
                            style: TextStyle(color: Colors.black87),
                            decoration: InputDecoration(
                                enabledBorder: new OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(8.0),
                                  borderSide: BorderSide(color: Colors.black26),
                                ),
                                // contentPadding: EdgeInsets.all(20).0),
                                // Padding(
                                //   padding:EdgeInsets.all(12.0),
                                //   child: buildEmail(),

                                // ),
                                hintText: 'Road Name,Area',
                                suffixIcon: IconButton(
                                    icon: Icon(Icons.search,
                                        color: Colors.black26)),
                                hintStyle: TextStyle(color: Colors.black38)),
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Container(
                          height: 40,
                          child: TextField(
                            keyboardType: TextInputType.emailAddress,
                            style: TextStyle(color: Colors.black87),
                            decoration: InputDecoration(
                                enabledBorder: new OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(8.0),
                                  borderSide: BorderSide(color: Colors.black26),
                                ),
                                // contentPadding: EdgeInsets.all(20).0),
                                // Padding(
                                //   padding:EdgeInsets.all(12.0),
                                //   child: buildEmail(),

                                // ),
                                hintText:
                                    'Add Nearby Famous Shop/Mall/Landmarks',
                                suffixIcon: IconButton(
                                    icon:
                                        Icon(Icons.add, color: Colors.black26)),
                                hintStyle: TextStyle(color: Colors.black38)),
                          ),
                        ),
                        SizedBox(
                          height: 9,
                        ),
                        Text(
                          "Type of Address",
                          style: TextStyle(
                              fontSize: 12,
                              color: Colors.black38,
                              letterSpacing: 1),
                        ),
                        Padding(padding: EdgeInsets.only(left: 6, right: 19)),
                        SizedBox(
                          height: 10,
                        ),
                        Row(children: [
                          new Flexible(
                            child: Container(
                              height: 40,
                              width: 118,

                              child: TextField(
                                keyboardType: TextInputType.emailAddress,
                                style: TextStyle(color: Colors.red[900]),
                                decoration: InputDecoration(
                                    enabledBorder: new OutlineInputBorder(
                                      borderRadius:
                                          new BorderRadius.circular(25.0),
                                      borderSide:
                                          BorderSide(color: Colors.red[900]),
                                    ),
                                    // contentPadding: EdgeInsets.all(20).0),
                                    // Padding(
                                    //   padding:EdgeInsets.all(12.0),
                                    //   child: buildEmail(),

                                    // ),
                                    labelText: 'Home',
                                    suffixIcon: IconButton(
                                        icon: Icon(Icons.home,
                                            color: Colors.black26)),
                                    hintStyle:
                                        TextStyle(color: Colors.black38)),
                              ),

                              // padding: EdgeInsets.only(left: 1, right: 9),

                              // margin: EdgeInsets.only(right: 160),
                            ),
                          ),
                          SizedBox(width: 10),
                          new Flexible(
                            child: Container(
                              height: 40,
                              width: 120,

                              child: TextField(
                                keyboardType: TextInputType.emailAddress,
                                style: TextStyle(color: Colors.black87),
                                decoration: InputDecoration(
                                    suffixIcon: IconButton(
                                        icon: Icon(Icons.local_post_office,
                                            color: Colors.black26)),
                                    enabledBorder: new OutlineInputBorder(
                                      borderRadius:
                                          new BorderRadius.circular(25.0),
                                      borderSide:
                                          BorderSide(color: Colors.black26),
                                    ),

                                    // contentPadding: EdgeInsets.all(20).0),
                                    // Padding(
                                    //   padding:EdgeInsets.all(12.0),
                                    //   child: buildEmail(),

                                    // ),

                                    // suffixIcon: IconButton(
                                    //     icon: Icon(Icons.local_post_office,
                                    //         color: Colors.black26)),
                                    labelText: 'Office',
                                    hintStyle:
                                        TextStyle(color: Colors.black38)),
                              ),
                              // padding: EdgeInsets.only(left: 1, right: 9),

                              // margin: EdgeInsets.only(right: 160),
                            ),
                          ),
                        ]),
                        SizedBox(height: 10),
                        Container(
                          decoration: BoxDecoration(
                              gradient: LinearGradient(colors: [
                            Color(0xFFD63027),
                            Color(0xFFD63027),
                            Color(0xFFAE1D3D),
                            Color(0xFF8E0F50),
                          ])),
                          child: TextButton(
                              style: TextButton.styleFrom(
                                primary: Colors.white,
                                padding: EdgeInsets.symmetric(
                                    vertical: 2, horizontal: 110),
                              ),
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => NewAddress(),
                                  ),
                                );
                              },
                              child: Text(
                                'SAVE ADDRESS'.toString(),
                                style: TextStyle(fontSize: 12),
                              )),
                        )
                      ],
                    ),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Color(0xFFF5F5F5F5)),
                    // alignment: Alignment.centerRight,

                    margin: EdgeInsets.only(left: 22, right: 22),

                    padding: EdgeInsets.only(
                      left: 20,
                      right: 20,
                    ),

                    height: 700,
                  )),

              // child: Icons.arrow_forward_ios

              //  child:  static const IconData arrow_forward_ios = IconData(0xe5b0, fontFamily: 'MaterialIcons', matchTextDirection: true);
            ]),

            //         SizedBox(
            //           height: 0,
            //         ),
            //         Padding(
            //           padding: const EdgeInsets.all(8.0),
            //           child: Container(
            //               child: Text(
            //             "Mobile No",
            //             style: TextStyle(
            //                 fontSize: 20, color: Colors.white),
            //           )),
            //         ),

            //         SizedBox(
            //           height: 0,
            //         ),
            //         // Padding(padding: const EdgeInsets.only(left: 15, right: 9)),
            //         Padding(
            //           padding: const EdgeInsets.all(8.0),
            //           child: Container(
            //               child: Text(
            //             "Mail Id",
            //             style: TextStyle(
            //                 fontSize: 20, color: Colors.white),
            //           )),
            //         ),
            //       ]),
            // ),
          ),

          // SizedBox(height: 1),
        ]));
  }
}
