/// Flutter code sample for ExpansionPanelList

// Here is a simple example of how to implement ExpansionPanelList.

import 'package:flutter/material.dart';

// stores ExpansionPanel state information
class Item {
  Item({
    @required this.expandedValue,
    @required this.headerValue,
    this.isExpanded = false,
  });

  Widget expandedValue;
  String headerValue;
  bool isExpanded;
}

List<Item> generateItems(int numberOfItems) {
  return List<Item>.generate(numberOfItems, (int index) {
    return Item(
      headerValue: 'CASH ON DELIVERY',
      expandedValue: Text(
        '234542',
        style: TextStyle(
          fontSize: 70,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  });
}

/// This is the stateful widget that the main application instantiates.
class CashOnDelivery extends StatefulWidget {
  const CashOnDelivery({Key key}) : super(key: key);

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

/// This is the private State class that goes with CashOnDelivery.
class _MyStatefulWidgetState extends State<CashOnDelivery> {
  final List<Item> _data = generateItems(1);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: _buildPanel(),
      ),
    );
  }

  Widget _buildPanel() {
    return ExpansionPanelList(
      expansionCallback: (int index, bool isExpanded) {
        setState(() {
          _data[index].isExpanded = !isExpanded;
        });
      },
      children: _data.map<ExpansionPanel>((Item item) {
        return ExpansionPanel(
          headerBuilder: (BuildContext context, bool isExpanded) {
            return Container(
              padding: EdgeInsets.symmetric(horizontal: 7),
              child: Row(
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Image.asset(
                    'assets/images/cod.png',
                    height: 20,
                    width: 20,
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Text(
                    'CASH ON DELIVERY',
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                  )
                ],
              ),
            );
          },
          body: Container(
            child: Column(
              children: [
                Text(
                  '234542',
                  style: TextStyle(fontSize: 70),
                ),
                Container(
                  padding: EdgeInsets.only(left: 40, right: 40),
                  // padding: EdgeInsets.only(bottom: 10),
                  child: TextField(
                    // keyboardType: TextInputType.,

                    // keyboardType: TextInputType.

                    style: TextStyle(color: Colors.black87),

                    decoration: InputDecoration(
                        enabledBorder: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(8.0),
                          borderSide: BorderSide(color: Colors.black26),
                        ),
                        hintText: "Enter Code Number",
                        hintStyle: TextStyle(color: Colors.black38)),
                  ),
                ),
                SizedBox(height: 10),
                Row(
                  children: [
                    Padding(padding: EdgeInsets.only(left: 100)),
                    Text(
                      "CANCEL",
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          color: Colors.black38,
                          letterSpacing: 0),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    TextButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => CashOnDelivery(),
                          ),
                        );
                      },
                      child: Container(

                          // color: Colors.white,
                          height: 40,
                          child: Center(
                            child: Text(
                              "OK",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18),
                            ),
                          ),
                          padding:
                              EdgeInsets.symmetric(horizontal: 15, vertical: 6),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(6),
                            color: Colors.grey,
                          )),
                    )
                  ],
                ),
                SizedBox(
                  height: 5,
                )
              ],
            ),

            // margin: EdgeInsets.only(left: 4, right: 3),
          ),
          isExpanded: item.isExpanded,
        );
      }).toList(),
    );
  }
}
