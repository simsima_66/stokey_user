// import 'package:flutter/material.dart';

// // stores ExpansionPanel state information
// class Item {
//   Item({
//     @required this.expandedValue,
//     @required this.headerValue,
//     this.isExpanded = false,
//   });

//   Widget expandedValue;
//   String headerValue;
//   bool isExpanded;
// }

// List<Item> generateItems(int numberOfItems) {
//   return List<Item>.generate(numberOfItems, (int index) {
//     return Item(
//       headerValue: 'CREDIT/DEBIT CARD',
//       expandedValue: Text(
//         '234542',
//         style: TextStyle(
//           fontSize: 70,
//           fontWeight: FontWeight.bold,
//         ),
//       ),
//     );
//   });
// }

// /// This is the stateful widget that the main application instantiates.
// class CreditOrDebitCard extends StatefulWidget {
//   const CreditOrDebitCard({Key key}) : super(key: key);

//   @override
//   _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
// }

// /// This is the private State class that goes with CashOnDelivery.
// class _MyStatefulWidgetState extends State<CreditOrDebitCard> {
//   final List<Item> _data = generateItems(1);

//   @override
//   Widget build(BuildContext context) {
//     return SingleChildScrollView(
//       child: Container(
//         child: _buildPanel(),
//       ),
//     );
//   }

//   Widget _buildPanel() {
//     return ExpansionPanelList(
//       expansionCallback: (int index, bool isExpanded) {
//         setState(() {
//           _data[index].isExpanded = !isExpanded;
//         });
//       },
//       children: _data.map<ExpansionPanel>((Item item) {
//         return ExpansionPanel(
//           headerBuilder: (BuildContext context, bool isExpanded) {
//             return Container(
//               padding: EdgeInsets.symmetric(horizontal: 7),
//               child: Row(
//                 // mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 children: [
//                   Image.asset(
//                     'assets/images/credit-card.png',
//                     height: 20,
//                     width: 20,
//                   ),
//                   SizedBox(
//                     width: 16,
//                   ),
//                   Text(
//                     'CREDIT/DEBIT CARD',
//                     style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
//                   )
//                 ],
//               ),
//             );
//           },
//           body: Container(
//             padding: EdgeInsets.only(left: 40, right: 40),
//             child: Column(
//               children: [
//                 Padding(padding: EdgeInsets.only(left: 40, right: 20)),
//                 TextField(
//                   // keyboardType: TextInputType.,

//                   // keyboardType: TextInputType.

//                   style: TextStyle(color: Colors.black87),

//                   decoration: InputDecoration(
//                       enabledBorder: new OutlineInputBorder(
//                         borderRadius: new BorderRadius.circular(8.0),
//                         borderSide: BorderSide(color: Colors.black26),
//                       ),
//                       hintText: "Card Number",
//                       hintStyle: TextStyle(color: Colors.black38)),
//                 ),
//                 SizedBox(
//                   height: 10,
//                 ),
//                 Container(
//                   // padding: EdgeInsets.only(left: 2, right: 2),
//                   padding: EdgeInsets.all(1),
//                   // padding: EdgeInsets.only(bottom: 10),
//                   child: TextField(
//                     // keyboardType: TextInputType.,

//                     // keyboardType: TextInputType.

//                     style: TextStyle(color: Colors.black87),

//                     decoration: InputDecoration(
//                         enabledBorder: new OutlineInputBorder(
//                           borderRadius: new BorderRadius.circular(8.0),
//                           borderSide: BorderSide(color: Colors.black26),
//                         ),
//                         hintText: "Name On Card",
//                         hintStyle: TextStyle(color: Colors.black38)),
//                   ),
//                 ),
//                 Row(children: [
//                   Padding(padding: EdgeInsets.all(20)),
//                   TextField(
//                     // keyboardType: TextInputType.,

//                     // keyboardType: TextInputType.

//                     style: TextStyle(color: Colors.black87),

//                     decoration: InputDecoration(
//                         enabledBorder: new OutlineInputBorder(
//                           borderRadius: new BorderRadius.circular(8.0),
//                           borderSide: BorderSide(color: Colors.black26),
//                         ),
//                         hintText: "Name On Card",
//                         hintStyle: TextStyle(color: Colors.black38)),
//                   ),
//                   SizedBox(
//                     width: 20,
//                   ),
//                   TextField(
//                     // keyboardType: TextInputType.,

//                     // keyboardType: TextInputType.

//                     style: TextStyle(color: Colors.black87),

//                     decoration: InputDecoration(
//                         enabledBorder: new OutlineInputBorder(
//                           borderRadius: new BorderRadius.circular(8.0),
//                           borderSide: BorderSide(color: Colors.black26),
//                         ),
//                         hintText: "Name On Card",
//                         hintStyle: TextStyle(color: Colors.black38)),
//                   ),
//                   Padding(
//                     padding: EdgeInsets.all(20),
//                   )
//                 ]),
//                 SizedBox(height: 10),
//                 Row(
//                   children: [
//                     Padding(padding: EdgeInsets.only(left: 100)),
//                     Text(
//                       "CANCEL",
//                       style: TextStyle(
//                           fontSize: 18,
//                           fontWeight: FontWeight.bold,
//                           color: Colors.black38,
//                           letterSpacing: 0),
//                     ),
//                     SizedBox(
//                       width: 20,
//                     ),
//                     TextButton(
//                       onPressed: () {
//                         Navigator.push(
//                           context,
//                           MaterialPageRoute(
//                             builder: (context) => CreditOrDebitCard(),
//                           ),
//                         );
//                       },
//                       child: Container(

//                           // color: Colors.white,
//                           height: 40,
//                           child: Center(
//                             child: Text(
//                               "OK",
//                               style: TextStyle(
//                                   color: Colors.white,
//                                   fontWeight: FontWeight.bold,
//                                   fontSize: 18),
//                             ),
//                           ),
//                           padding:
//                               EdgeInsets.symmetric(horizontal: 15, vertical: 6),
//                           decoration: BoxDecoration(
//                             borderRadius: BorderRadius.circular(6),
//                             color: Colors.grey,
//                           )),
//                     )
//                   ],
//                 ),
//                 SizedBox(
//                   height: 5,
//                 )
//               ],
//             ),

//             // margin: EdgeInsets.only(left: 4, right: 3),
//           ),
//           isExpanded: item.isExpanded,
//         );
//       }).toList(),
//     );
//   }
// }

import 'package:flutter/material.dart';
import 'package:flutter_credit_card/credit_card_form.dart';
import 'package:flutter_credit_card/credit_card_widget.dart';
import 'package:flutter_credit_card/flutter_credit_card.dart';

// stores ExpansionPanel state information
class Item {
  Item({
    @required this.expandedValue,
    @required this.headerValue,
    this.isExpanded = false,
  });

  Widget expandedValue;
  String headerValue;
  bool isExpanded;
}

List<Item> generateItems(int numberOfItems) {
  return List<Item>.generate(numberOfItems, (int index) {
    return Item(
      headerValue: 'CREDIT/DEBIT CARD',
      expandedValue: Text(
        '234542',
        style: TextStyle(
          fontSize: 70,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  });
}

/// This is the stateful widget that the main application instantiates.
class CreditOrDebitCard extends StatefulWidget {
  const CreditOrDebitCard({Key key}) : super(key: key);

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

/// This is the private State class that goes with CashOnDelivery.
class _MyStatefulWidgetState extends State<CreditOrDebitCard> {
  final List<Item> _data = generateItems(1);

  String cardNumber = '';
  String expiryDate = '';
  String cardHolderName = '';
  String cvvCode = '';
  bool isCvvFocused = false;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: _buildPanel(),
      ),
    );
  }

  Widget _buildPanel() {
    return ExpansionPanelList(
      expansionCallback: (int index, bool isExpanded) {
        setState(() {
          _data[index].isExpanded = !isExpanded;
        });
      },
      children: _data.map<ExpansionPanel>((Item item) {
        return ExpansionPanel(
          headerBuilder: (BuildContext context, bool isExpanded) {
            return Container(
              padding: EdgeInsets.symmetric(horizontal: 7),
              child: Row(
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Image.asset(
                    'assets/images/credit-card.png',
                    height: 20,
                    width: 20,
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Text(
                    'CREDIT/DEBIT CARD',
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                  )
                ],
              ),
            );
          },
          body: Container(
            padding: EdgeInsets.only(left: 40, right: 40),

            child: Column(
              children: [
                CreditCardWidget(
                  cardNumber: cardNumber,
                  expiryDate: expiryDate,
                  cardHolderName: cardHolderName,
                  cvvCode: cvvCode,
                  showBackView: isCvvFocused,
                  // cardbgColor: Colors.black,
                  obscureCardNumber: true,
                  obscureCardCvv: true,
                  height: 175,
                  textStyle: TextStyle(color: Colors.yellowAccent),
                  width: MediaQuery.of(context).size.width,
                  animationDuration: Duration(milliseconds: 1000),
                ),
                CreditCardForm(
                  formKey: formKey, // Required
                  onCreditCardModelChange:
                      (CreditCardModel data) {}, // Required
                  themeColor: Colors.red,
                  obscureCvv: true,
                  obscureNumber: true,
                  cardNumberDecoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Number',
                    hintText: 'XXXX XXXX XXXX XXXX',
                  ),
                  expiryDateDecoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Expired Date',
                    hintText: 'XX/XX',
                  ),
                  cvvCodeDecoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'CVV',
                    hintText: 'XXX',
                  ),
                  cardHolderDecoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Card Holder',
                  ),
                ),
              ],
            ),

            // child: Column(
            //   children: [
            //     // Padding(padding: EdgeInsets.only(left: 40, right: 40)),
            //     TextField(
            //       // keyboardType: TextInputType.,

            //       // keyboardType: TextInputType.

            //       style: TextStyle(color: Colors.black87),

            //       decoration: InputDecoration(
            //           enabledBorder: new OutlineInputBorder(
            //             borderRadius: new BorderRadius.circular(8.0),
            //             borderSide: BorderSide(color: Colors.black26),
            //           ),
            //           hintText: "Card Number",
            //           hintStyle: TextStyle(color: Colors.black38)),
            //     ),

            //     SizedBox(
            //       height: 10,
            //     ),
            //     Container(
            //       padding: EdgeInsets.only(left: 1, right: 1),
            //       // padding: EdgeInsets.only(bottom: 10),
            //       child: TextField(
            //         // keyboardType: TextInputType.,

            //         // keyboardType: TextInputType.

            //         style: TextStyle(color: Colors.black87),

            //         decoration: InputDecoration(
            //             enabledBorder: new OutlineInputBorder(
            //               borderRadius: new BorderRadius.circular(8.0),
            //               borderSide: BorderSide(color: Colors.black26),
            //             ),
            //             hintText: "Name On Card",
            //             hintStyle: TextStyle(color: Colors.black38)),
            //       ),
            //     ),

            //     SizedBox(
            //       height: 10,
            //     ),

            //     Row(children: [
            //       new Flexible(
            //         child: Container(
            //           height: 40,

            //           child: TextField(
            //             keyboardType: TextInputType.emailAddress,
            //             style: TextStyle(color: Colors.black87),
            //             decoration: InputDecoration(
            //                 enabledBorder: new OutlineInputBorder(
            //                   borderRadius: new BorderRadius.circular(8.0),
            //                   borderSide: BorderSide(color: Colors.black26),
            //                 ),

            //                 // contentPadding: EdgeInsets.all(20).0),
            //                 // Padding(
            //                 //   padding:EdgeInsets.all(12.0),
            //                 //   child: buildEmail(),

            //                 // ),
            //                 hintText: 'Expiration',
            //                 hintStyle: TextStyle(color: Colors.black38)),
            //           ),
            //           // padding: EdgeInsets.only(left: 1, right: 9),

            //           // margin: EdgeInsets.only(right: 160),
            //         ),
            //       ),
            //       SizedBox(width: 20),
            //       new Flexible(
            //         child: Container(
            //           height: 40,

            //           child: TextField(
            //             keyboardType: TextInputType.emailAddress,
            //             style: TextStyle(color: Colors.black87),
            //             decoration: InputDecoration(
            //                 enabledBorder: new OutlineInputBorder(
            //                   borderRadius: new BorderRadius.circular(8.0),
            //                   borderSide: BorderSide(color: Colors.black26),
            //                 ),
            //                 // contentPadding: EdgeInsets.all(20).0),
            //                 // Padding(
            //                 //   padding:EdgeInsets.all(12.0),
            //                 //   child: buildEmail(),

            //                 // ),

            //                 suffixIcon: IconButton(
            //                     icon: Icon(Icons.question_answer_outlined,
            //                         color: Colors.black26)),
            //                 hintText: 'CCV',
            //                 hintStyle: TextStyle(color: Colors.black38)),
            //           ),
            //           // padding: EdgeInsets.only(left: 1, right: 9),

            //           // margin: EdgeInsets.only(right: 160),
            //         ),
            //       ),
            //     ]),

            //     SizedBox(height: 80),
            //     Row(
            //       children: [
            //         Padding(padding: EdgeInsets.only(left: 100)),
            //         Text(
            //           "CANCEL",
            //           style: TextStyle(
            //               fontSize: 18,
            //               fontWeight: FontWeight.bold,
            //               color: Colors.black38,
            //               letterSpacing: 0),
            //         ),
            //         SizedBox(
            //           width: 20,
            //         ),
            //         TextButton(
            //           onPressed: () {
            //             Navigator.push(
            //               context,
            //               MaterialPageRoute(
            //                 builder: (context) => CreditOrDebitCard(),
            //               ),
            //             );
            //           },
            //           child: Container(

            //               // color: Colors.white,
            //               height: 40,
            //               child: Center(
            //                 child: Text(
            //                   "OK",
            //                   style: TextStyle(
            //                       color: Colors.white,
            //                       fontWeight: FontWeight.bold,
            //                       fontSize: 18),
            //                 ),
            //               ),
            //               padding:
            //                   EdgeInsets.symmetric(horizontal: 15, vertical: 6),
            //               decoration: BoxDecoration(
            //                 borderRadius: BorderRadius.circular(6),
            //                 color: Colors.grey,
            //               )),
            //         )
            //       ],
            //     ),
            //     SizedBox(
            //       height: 5,
            //     ),
            //   ],
            // ),

            // margin: EdgeInsets.only(left: 4, right: 3),
          ),
          isExpanded: item.isExpanded,
        );
      }).toList(),
    );
  }
}
