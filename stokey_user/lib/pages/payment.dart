import 'package:flutter/material.dart';
import 'package:stokey_user/pages/NetBanking.dart';
import 'package:stokey_user/pages/CheckOut.dart';
import 'package:stokey_user/pages/Paytm.dart';
import 'package:stokey_user/pages/PhonePee.dart';
import 'package:stokey_user/pages/cash_on_delivery.dart';
import 'package:stokey_user/pages/credit_debit_card.dart';
import 'package:stokey_user/pages/profile.dart';
import 'package:flutter_credit_card/flutter_credit_card.dart';

class Payment extends StatefulWidget {
  @override
  _PaymentState createState() => _PaymentState();
}

class _PaymentState extends State<Payment> {
  bool _checkboxValue = false;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(
            iconTheme: IconThemeData(
              color: Colors.black,
            ),
            backgroundColor: Colors.white,
            title: Text(
              'Payment',
              style: TextStyle(color: Colors.black),
            ),
          ),
          body: Column(children: [
            Container(
              height: 15,
              decoration: BoxDecoration(
                  gradient: LinearGradient(colors: [
                Color(0xFFD63027),
                Color(0xFFD63027),
                Color(0xFFAE1D3D),
                Color(0xFF8E0F50),
              ])),
            ),
            Container(
              decoration: BoxDecoration(
                boxShadow: [BoxShadow(blurRadius: 4)],
                color: Colors.white,
              ),
              // color: Colors.white,
              child: Row(
                children: [
                  SizedBox(
                    width: 15,
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        _checkboxValue = !_checkboxValue;
                      });
                    },
                    child: Checkbox(
                      value: _checkboxValue,
                      checkColor: Colors.black,
                      activeColor: Colors.red,
                      // onChanged: (bool value) {
                      //   print(value);
                      // },
                    ),
                  ),
                  Text("Story Points"),
                  SizedBox(
                    width: 30,
                  ),
                  VerticalDivider(),
                  Container(
                    width: 2,
                    height: 25,
                    color: Colors.black,
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Text('Total Points : '),
                  Container(
                    child: Center(
                      child: Image.asset(
                        'assets/images/reward.png',
                        height: 20,
                        width: 20,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    '5000',
                    style: TextStyle(color: Colors.red[900]),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 25,
            ),
            Container(
              alignment: Alignment.centerLeft,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Color(0xFFF5F5F5F5),
              ),

              // alignment: Alignment.centerRight,

              margin: EdgeInsets.only(left: 12, right: 10),

              padding: EdgeInsets.only(
                left: 2,
                right: 2,
              ),

              height: 322,

              child: ListView(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Row(
                      children: [
                        Expanded(
                            child: Text(
                          "Payment Options",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20),
                        )),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  CashOnDelivery(),
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Row(
                      children: [],
                    ),
                  ),
                  SizedBox(
                    height: 1,
                  ),
                  CreditOrDebitCard(),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 4),
                    child: Row(
                      children: [],
                    ),
                  ),
                  SizedBox(
                    height: 35,
                  ),
                  PhonePe(),
                  Padding(
                      padding: const EdgeInsets.only(bottom: 4),
                      child: Row(
                        children: [],
                      )),
                  SizedBox(
                    height: 35,
                  ),
                  Paytm(),
                  Padding(
                      padding: const EdgeInsets.only(bottom: 4),
                      child: Row(
                        children: [],
                      )),
                  SizedBox(
                    height: 35,
                  ),
                  NetBanking(),
                  Padding(
                      padding: const EdgeInsets.only(bottom: 4),
                      child: Row(
                        children: [],
                      )),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 4),
              child: Row(
                children: [],
              ),
            ),
            SizedBox(
              height: 49,
            ),
            Text(
              "Price Details(3 Item)",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
            ),
            SizedBox(
              height: 10,
            ),
            Column(children: [
              Row(
                children: [
                  SizedBox(
                    width: 20,
                  ),
                  Text("Total"),
                  SizedBox(
                    width: 280,
                  ),
                  Text(
                    '\u{20B9}50',
                    style: TextStyle(fontSize: 15),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  SizedBox(
                    width: 20,
                  ),
                  Text(
                    "Discount",
                    style: TextStyle(color: Colors.green),
                  ),
                  SizedBox(
                    width: 255,
                  ),
                  Text(
                    '\u{20B9}50',
                    style: TextStyle(fontSize: 15, color: Colors.green),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  SizedBox(
                    width: 20,
                  ),
                  Text("Delivery"),
                  SizedBox(
                    width: 260,
                  ),
                  Text(
                    'Free',
                    style: TextStyle(fontSize: 15),
                  ),
                ],
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                margin: EdgeInsets.only(left: 8, right: 8),
                child: Divider(
                  color: Colors.black,
                  height: 2,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  SizedBox(
                    width: 20,
                  ),
                  Text(
                    "Total Amount",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                  ),
                  SizedBox(
                    width: 215,
                  ),
                  Text(
                    '\u{20B9}50',
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ]),
            SizedBox(
              height: 40,
            ),
            Container(
                child: Row(
                  children: [
                    SizedBox(
                      width: 40,
                    ),
                    Text(
                      '\u{20B9}50.00',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 25),
                    ),
                    SizedBox(
                      width: 140,
                    ),
                    TextButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => OrderSuccessfull()),
                          );
                        },
                        child: Container(

                            // color: Colors.white,
                            height: 40,
                            child: Center(
                              child: Text(
                                "PLACE ORDER",
                                style: TextStyle(color: Colors.red[900]),
                              ),
                            ),
                            padding: EdgeInsets.only(left: 10, right: 10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(6),
                                color: Colors.white,
                                boxShadow: [BoxShadow(blurRadius: 3)]))),
                  ],
                ),

                // color: Colors.red[900],
                height: 95.2,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(25),
                        topRight: Radius.circular(25)),
                    gradient: LinearGradient(colors: [
                      Color(0xFFD63027),
                      Color(0xFFD63027),
                      Color(0xFFAE1D3D),
                      Color(0xFF8E0F50),
                    ])))
          ])),
    );
  }
}
