import 'package:flutter/material.dart';
import 'package:stokey_user/pages/payment.dart';
import 'package:stokey_user/pages/feedback.dart';

class OrderSuccessfull extends StatefulWidget {
  @override
  _OrderSuccessfullState createState() => _OrderSuccessfullState();
}

class _OrderSuccessfullState extends State<OrderSuccessfull> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
        backgroundColor: Colors.white,
        title: Text(
          'Check Out',
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: Container(
        color: Colors.white30,
        child: Column(children: [
          Container(
            height: 15,
            decoration: BoxDecoration(
                gradient: LinearGradient(colors: [
              Color(0xFFD63027),
              Color(0xFFD63027),
              Color(0xFFAE1D3D),
              Color(0xFF8E0F50),
            ])),
          ),
//             SizedBox(height: 80),
//             Container(
//               height: 500,
//               color: Colors.white,
//               child: Container(
//                 decoration: BoxDecoration(
//                   borderRadius: BorderRadius.all(Radius.circular(20)),
//                   color: Colors.white,
//                 ),
//                 margin: EdgeInsets.only(left: 20, right: 20),
//                 padding: EdgeInsets.only(
//                   left: 8,
//                   right: 8,
//                 ),
//                 height: 400,
//                 child: Padding(
//                   padding: const EdgeInsets.all(5.0),
//                   child: Row(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: [
//                       Padding(padding: const EdgeInsets.only(left: 2)),
//                       Padding(
//                         padding: const EdgeInsets.only(left: 5, right: 5),
//                         child: Column(
//                             crossAxisAlignment: CrossAxisAlignment.start,
//                             children: <Widget>[
//                               SizedBox(
//                                 height: 10,
//                               ),
//                               Column(children: [
//                                 Padding(
//                                   padding: const EdgeInsets.only(
//                                     left: 40,
//                                     right: 8,
//                                   ),

          SizedBox(
            height: 100,
          ),
          Center(
            child: Image.asset(
              'assets/images/orderSuc.png',
              height: 200,
              width: 200,
            ),
          ),

          SizedBox(
            height: 50,
          ),
          Container(
            padding: EdgeInsets.only(left: 40, right: 40),
            child: Text(
              'Order Successfull',
              style: TextStyle(
                  fontSize: 35,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
            ),
          ),
//                                 TextButton(
//                                   onPressed: () {},

//                                   // child: Icons.arrow_forward_ios

//                                   //  child:  static const IconData arrow_forward_ios = IconData(0xe5b0, fontFamily: 'MaterialIcons', matchTextDirection: true);
//                                 )
//                               ]),

//                               SizedBox(
//                                 height: 120,
//                               ),
//                               Padding(
//                                   padding: const EdgeInsets.only(
//                                       left: 30, right: 50)),
          SizedBox(
            height: 60,
          ),
          Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(colors: [
              Color(0xFFD63027),
              Color(0xFFD63027),
              Color(0xFFAE1D3D),
              Color(0xFF8E0F50),
            ])),
            padding: EdgeInsets.only(left: 30, right: 30),
            child: TextButton(
                style: TextButton.styleFrom(
                  primary: Colors.white,
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => FeedBack(),
                    ),
                  );
                },
                child: Text(
                  'TRACK YOUR ORDER'.toUpperCase(),
                  style: TextStyle(fontSize: 20),
                )),
          ),

          SizedBox(
            height: 160,
          ),

          // Padding(padding: const EdgeInsets.only(left: 15, right: 9)),
          Padding(
            padding: const EdgeInsets.only(right: 100, left: 50),
            child: TextButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Payment(),
                  ),
                );
              },
              child: Container(
                  padding: EdgeInsets.only(left: 50, right: 20),
                  child: Text(
                    "GO BACK",
                    style: TextStyle(fontSize: 18, color: Colors.black),
                  )),
            ),
          )
        ]),
      ),
    );
  }
}
