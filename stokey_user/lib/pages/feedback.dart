import 'package:flutter/material.dart';

class FeedBack extends StatefulWidget {
  @override
  _FeedbackState createState() => _FeedbackState();
}

class _FeedbackState extends State<FeedBack> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.black,
          ),
          title: Text('FeedBack',
              style: TextStyle(
                color: Colors.black,
              )),
          backgroundColor: Colors.white,
          elevation: 4.0),
      body: Container(
        color: Colors.white30,
        child: Column(children: [
          Container(
            child: Image.asset(
              'assets/images/people.png',
              height: 250,
              width: 300,
            ),
          ),
          Container(
            child: Stack(
              children: [
                // SizedBox(height: 40),
                // buildEmail(),
                // SizedBox(height: 50),

                SizedBox(height: 2),

                Container(
                  // alignment: Alignment.centerRight,
                  padding: EdgeInsets.only(
                    left: 2,
                    right: 2,
                  ),
                )
              ],
            ),
          ),
          SizedBox(height: 1),
          Column(children: [
            Container(
              height: 440,
              color: Colors.white,
              child: Stack(children: [
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    color: Color(0xFFF5F5F5F5),
                  ),
                  margin: EdgeInsets.only(left: 25, right: 25),
                  padding: EdgeInsets.only(
                    left: 12,
                    right: 12,
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(3.0),
                    child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  height: 150,
                                  child: Row(children: [
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Image.asset(
                                      'assets/images/favemo.png',
                                      height: 50,
                                      width: 50,
                                    ),
                                    SizedBox(
                                      width: 30,
                                    ),
                                    Image.asset(
                                      'assets/images/smile.png',
                                      height: 50,
                                      width: 50,
                                    ),
                                    SizedBox(
                                      width: 30,
                                    ),
                                    Image.asset(
                                      'assets/images/shutemo.png',
                                      height: 50,
                                      width: 50,
                                    ),
                                    SizedBox(
                                      width: 30,
                                    ),
                                    Image.asset(
                                      'assets/images/sad.png',
                                      height: 50,
                                      width: 50,
                                    ),
                                    SizedBox(
                                      width: 2,
                                    ),
                                  ]),
                                  // child: Icons.arrow_forward_ios
                                ) //  child:  static const IconData arrow_forward_ios = IconData(0xe5b0, fontFamily: 'MaterialIcons', matchTextDirection: true);
                              ]) // Padding(padding: const EdgeInsets.only(left: 15, right: 9)),
                        ]),
                  ),
                ),
                Positioned(
                    right: 60,
                    left: 60,
                    child: Container(
                        alignment: Alignment.centerLeft,
                        child: Column(children: <Widget>[
                          SizedBox(height: 150),
                          Container(
                            alignment: Alignment.centerLeft,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(5),
                            ),
                            height: 180,
                            child: Container(
                              child: TextField(
                                maxLines: 99,
                                decoration: InputDecoration(
                                  hintText: "Comment!",
                                  border: OutlineInputBorder(),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                          TextButton(
                              style: TextButton.styleFrom(
                                padding: EdgeInsets.only(left: 80, right: 80),
                              ),
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => FeedBack()),
                                );
                              },
                              child: Container(

                                  // color: Colors.white,
                                  height: 40,
                                  child: Center(
                                    child: Text(
                                      "ENTER",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  padding: EdgeInsets.only(left: 30, right: 30),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(6),
                                    gradient: LinearGradient(colors: [
                                      Color(0xFFD63027),
                                      Color(0xFFD63027),
                                      Color(0xFFAE1D3D),
                                      Color(0xFF8E0F50),
                                    ]),
                                  ))),
                        ])))
              ]),
            ),
          ])
        ]),
      ),
    );
  }
}
