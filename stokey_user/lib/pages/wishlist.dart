import 'package:flutter/material.dart';
import 'package:stokey_user/pages/coupon.dart';

class Wishlist extends StatefulWidget {
  @override
  _WishlistState createState() => _WishlistState();
}

class _WishlistState extends State<Wishlist> {
  @override

// class _perofileState extends State<Profile> {
//   @override
  // int _quantity = 0;
  Widget build(BuildContext context) {
    void add() {
      // setState(() {
      // _quantity++;
      // });
    }

    void remove() {
      // setState(() {
      // if (_quantity > 0) {
      // _quantity--;
      // }
      // });
    }

    return Scaffold(
      appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.black,
          ),
          title: Text('Wishlist',
              style: TextStyle(
                color: Colors.black,
              )),
          backgroundColor: Colors.white,
          elevation: 4.0),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              child: Stack(
                children: [
                  // SizedBox(height: 40),
                  // buildEmail(),
                  // SizedBox(height: 50),
                  Container(
                    decoration: BoxDecoration(
                        gradient: LinearGradient(colors: [
                      Color(0xFFD63027),
                      Color(0xFFD63027),
                      Color(0xFFAE1D3D),
                      Color(0xFF8E0F50),
                    ])),
                    child: TextButton(
                      style: TextButton.styleFrom(
                        primary: Colors.white,
                        onSurface: Colors.deepOrange[900],
                        padding:
                            EdgeInsets.symmetric(vertical: 30, horizontal: 300),
                      ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => Wishlist(),
                          ),
                        );
                      },
                      child: null,
                    ),
                  ),

                  buildEmail(),

                  Container(
                    // alignment: Alignment.centerRight,
                    padding: EdgeInsets.only(
                      left: 2,
                      right: 2,
                    ),
                  )
                ],
              ),
            ),
            ListItems(
                name: "carrot", price: "\u{20B9}160", perkg: "\u{20B9}50/kg"),
            ListItems(
                name: "potato", price: "\u{20B9}130", perkg: "\u{20B9}30/kg"),
            ListItems(
                name: "potato", price: "\u{20B9}140", perkg: "\u{20B9}70/kg"),
            ListItems(
                name: "potato", price: "\u{20B9}170", perkg: "\u{20B9}20/kg"),
            ListItems(
                name: "potato", price: "\u{20B9}130", perkg: "\u{20B9}40/kg")
          ],
        ),
      ),
    );
  }
}

class ListItems extends StatefulWidget {
  const ListItems({Key key, @required this.name, this.price, this.perkg});

  final String name;

  final String price;

  final String perkg;

  @override
  _ListItemsState createState() => _ListItemsState();
}

class _ListItemsState extends State<ListItems> {
  var _quantity = 0;

  // final int _quantity;

  // final Function add;

  // final Function remove;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(children: [
        SizedBox(
          height: 30,
        ),
        Container(
          height: 100,
          margin: EdgeInsets.only(left: 10, right: 10),
          decoration: BoxDecoration(
            color: Color(0xFFF6F5FA),
            borderRadius: BorderRadius.all(
              Radius.circular(15),
            ),
          ),
          child: Row(children: [
            Container(
              // padding: EdgeInsets.only(left: 5, right: 5),
              margin: EdgeInsets.only(left: 5, right: 5),
              decoration: BoxDecoration(
                border: Border.all(color: Colors.red),
                color: Colors.white,
              ),
              child: Row(
                children: [
                  Image.asset(
                    'assets/images/carrot.png',
                    height: 45,
                    width: 45,
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 5,
            ),
            SizedBox(
              width: 10,
            ),
            Column(
              children: [
                SizedBox(
                  height: 30,
                ),
                Container(
                  child: Text(
                    widget.name,
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Colors.black),
                  ),
                ),
                Text(
                  widget.perkg,
                  style: TextStyle(fontSize: 14, color: Colors.black),
                ),
              ],
            ),
            SizedBox(
              width: 40,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "(Quantity)",
                  style: TextStyle(fontSize: 10),
                ),
                SizedBox(
                  height: 3,
                ),
                Container(
                  child: Row(
                    children: [
                      GestureDetector(
                        onTap: () => setState(() {
                          _quantity++;
                        }),
                        child: Container(
                          padding: EdgeInsets.all(1),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              gradient: LinearGradient(colors: [
                                Color(0xFFD63027),
                                Color(0xFFD63027),
                                Color(0xFFAE1D3D),
                                Color(0xFF8E0F50),
                              ])),
                          height: 25,
                          width: 25,
                          child: Icon(
                            Icons.add,
                            color: Colors.white,
                            size: 15,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 7,
                      ),
                      Container(
                        child: Text(
                          _quantity.toString(),
                          style: TextStyle(fontSize: 17),
                        ),
                      ),
                      // Padding(
                      //   padding: EdgeInsets.only(left: 2),
                      // ),
                      SizedBox(
                        width: 7,
                      ),
                      GestureDetector(
                        onTap: () {
                          _quantity--;
                        },
                        child: Container(
                          padding: EdgeInsets.all(1),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              gradient: LinearGradient(colors: [
                                Color(0xFFD63027),
                                Color(0xFFD63027),
                                Color(0xFFAE1D3D),
                                Color(0xFF8E0F50),
                              ])),
                          height: 25,
                          width: 25,
                          child: Icon(
                            Icons.remove,
                            color: Colors.white,
                            size: 15,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Padding(padding: EdgeInsets.only(left: 20)),
            SizedBox(
              width: 30,
            ),
            Column(
              children: [
                SizedBox(
                  height: 50,
                ),
                Container(
                  child: Text(
                    widget.price,
                    style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.bold,
                        color: Colors.black),
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      gradient: LinearGradient(colors: [
                        Color(0xFFD63027),
                        Color(0xFFD63027),
                        Color(0xFFAE1D3D),
                        Color(0xFF8E0F50),
                      ])),
                  child: TextButton(
                    style: TextButton.styleFrom(
                      primary: Colors.white,

                      // backgroundColor: Colors.deepOrange[900],

                      // onSurface: Colors.deepOrange[900],
                      // padding: EdgeInsets.only(top: 2),
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => CartPage(),
                        ),
                      );
                    },
                    child: Text(
                      'ADD TO CART'.toUpperCase(),
                      style: TextStyle(fontSize: 10),
                    ),
                  ),
                  padding: EdgeInsets.only(left: 2, right: 2),
                  height: 25,
                ),
              ],
            ),
          ]),
        )
      ]),
    );
  }
}

Widget buildEmail() {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.center,
    children: <Widget>[
      SizedBox(height: 10),
      Container(
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5),
        ),
        margin: EdgeInsets.only(
          left: 30,
          right: 30,
        ),
        height: 40,
        child: TextField(
          keyboardType: TextInputType.emailAddress,
          style: TextStyle(color: Colors.black87),
          decoration: InputDecoration(
              enabledBorder: new OutlineInputBorder(
                borderRadius: new BorderRadius.circular(8.0),
                borderSide: BorderSide(color: Colors.black26),
              ),
              hintText: 'Search',
              suffixIcon:
                  IconButton(icon: Icon(Icons.search, color: Colors.black87)),
              hintStyle: TextStyle(color: Colors.black38)),
        ),
      )
    ],
  );
}
